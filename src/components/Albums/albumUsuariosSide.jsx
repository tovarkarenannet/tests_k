import { Fragment, React, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDeleteLeft, faXmarkCircle } from "@fortawesome/free-solid-svg-icons";
import {
  SideBar,
  SideBody,
  SideHead,
  PostTableAlbum,
  Button,
} from "../../styledComponents/styleComponentSide";
import { ContenedorTabla } from "../../styledComponents/styleComponentUsers";

const Side = (props) => {
  const usuarioAlbumArray = props.currentAlbumId;
  console.log("Entré en side Album: ", usuarioAlbumArray);

//   const [postDatas, setpostData] = useState(postArray);
//   const eliminarPhost = (id) => {
//     setpostData(postArray.filter((pst) => pst.id !== id));
//     console.log("Eliminar", setpostData);
//   };

  return (
    <Fragment>
      <SideBar>
        <SideHead>
          <div>
            <Button
              onClick={() => {
                props.setShowAlbumSide(false);
              }}
            >
              <FontAwesomeIcon icon={faXmarkCircle} />
            </Button>
          </div>
        </SideHead>
        <SideBody>
          <PostTableAlbum>
            <table>
              <tbody>
                {usuarioAlbumArray.map((item, index) => (
                  <tr key={item.id}>
                    <td>{index + 1}</td>
                    <td>{item.title}</td>
                    <td><img src={item.url} /></td>
                    <td><img src={item.thumbnailUrl} /></td>
                  </tr>
                ))}
              </tbody>
            </table>
          </PostTableAlbum>
        </SideBody>
      </SideBar>
    </Fragment>
  );
};

export default Side;
