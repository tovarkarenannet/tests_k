import { Fragment, React, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhotoFilm } from "@fortawesome/free-solid-svg-icons";
import album from "../../images/album.png";

//Componentes
import SideAlbumUsers from "./albumUsuariosSide";

//Servicio
import { obtenerAlbums } from "../../redux/albumServiceUsuarios";
import { obtenerAlbumsUser } from "../../redux/albumsService";

//StyledComponents
import {
  Content,
  Titulos,
  ContenedorTabla,
} from "../../styledComponents/styleComponentUsers";

import { Button } from "../../styledComponents/stylesComponentsLogin";

const Album = () => {
  const dispatchAlbums = useDispatch();
  const albumGet = useSelector((album) => album.albumServ.arrayAlbum);

  const albumGetUser = useSelector(
    (albumtU) => albumtU.albumUserServ.arrayAlbumOfUser
  );
  console.log("Soy los álbums de los Usuarios: ", albumGetUser);

  useEffect(() => {
    dispatchAlbums(obtenerAlbums());
    dispatchAlbums(obtenerAlbumsUser());
  }, [dispatchAlbums]);

  const [stateShowAlbumSide, setShowAlbumSide] = useState(false);

  const [currentAlbumId, setCurrentAlbumId] = useState(0);
  const presentarAlbum = (id) => {
    setShowAlbumSide(true);
    //setCurrentAlbumId(id);
    setCurrentAlbumId(albumGetUser.filter((albumU) => albumU.albumId === id));
  };

  if (albumGet === undefined) {
    return (
      <Fragment>
        <Content>
          <Titulos>
            Álbum <hr />
          </Titulos>
        </Content>
      </Fragment>
    );
  }

  return (
    <Fragment>
      {stateShowAlbumSide ? (
        <SideAlbumUsers
          currentAlbumId={currentAlbumId}
          setShowAlbumSide={setShowAlbumSide}
        />
      ) : null}
      <Content>
        <Titulos>
          Álbum <hr />
        </Titulos>
        <ContenedorTabla>
          <table>
            <tbody>
              {albumGet.map((item) => (
                <tr key={item.id}>
                  <td>
                    <div>
                      <img src={album} />
                      <b>
                        <h3>{item.name}</h3>
                      </b>
                      <h5>
                        <span>
                          <b>{item.username}</b>
                        </span>
                        <span>
                          <i>
                            <b>{item.email}</b>
                          </i>
                        </span>
                      </h5>
                      <span>
                        <i>
                          <h6>
                            {item.address.street} {item.address.suite}{" "}
                            {item.address.city} {item.address.zipcode}{" "}
                            {item.address.geo.lat} {item.address.geo.lng}
                          </h6>
                        </i>
                      </span>
                    </div>
                  </td>
                  <td>
                    <div id="acciones">
                      {stateShowAlbumSide ? (
                        <Button
                          title="Consultar Álbum"
                          onClick={() => setShowAlbumSide(false)}
                        >
                          <FontAwesomeIcon icon={faPhotoFilm} />
                        </Button>
                      ) : (
                        <Button
                          title="Consultar Álbum"
                          onClick={() => presentarAlbum(item.id)}
                        >
                          <FontAwesomeIcon icon={faPhotoFilm} />
                        </Button>
                      )}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </ContenedorTabla>
      </Content>
    </Fragment>
  );
};

export default Album;
