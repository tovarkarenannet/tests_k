import { Fragment, React, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faNoteSticky, faEdit } from "@fortawesome/free-solid-svg-icons";

//Componentes
import Side from "./sideUsuarios";

//Servicio
import { obtenerUsuarios } from "../../redux/usuariosServiceD";
import { obtenerPost } from "../../redux/postService";

//StyledComponents
import {
  Content,
  ContenedorTabla,
  Titulos,
} from "../../styledComponents/styleComponentUsers";
import { Button } from "../../styledComponents/stylesComponentsLogin";

const Usuarios = () => {
  const dispatch = useDispatch();
  const userGet = useSelector((user) => user.usuariosServ.arrayUsuarios);

  const postGet = useSelector((post) => post.postServ.arrayPost);
  console.log("Soy los post",postGet);

  useEffect(() => {
    dispatch(obtenerUsuarios());
    dispatch(obtenerPost());

  }, [dispatch]);

  const [stateShowSide, setShowSide] = useState(false);

  const [currentId, setCurrentId] = useState(0);
  const presentarUsuarios = (id) => {
    setShowSide(true);
    //setCurrentId(id);
    setCurrentId(userGet.filter((use) => use.id === id));
  };

  const [postId, setPostId] = useState(0);
  const presentarPost = (id) => {
    setPostId(postGet.filter((pos) => pos.userId === id));
  };

  if (userGet === undefined) {
    return (
      <Fragment>
        <Content>
          <Titulos>
            Usuarios <hr />
          </Titulos>
        </Content>
      </Fragment>
    );
  }

  return (
    <Fragment>
      {stateShowSide ? <Side currentId={currentId} setPostId={postId} setShowSide={setShowSide}/> : null}
      <Content>
        <Titulos>
          Usuarios <hr />
        </Titulos>
        <ContenedorTabla>
          <table>
            <tbody>
              {userGet.map((item) => (
                <tr key={item.id}>
                  <td>
                    <div>
                      <img src={item.avatar} />
                      <b><h3>{item.first_name} {item.last_name}</h3>
                        
                      </b>
                      <span>
                        <i>{item.email}</i>
                      </span>
                    </div>
                  </td>
                  <td>
                    <div>
                      <Button title="Editar">
                        <FontAwesomeIcon icon={faEdit} />
                      </Button>
                      {stateShowSide ? (
                        <Button title="Post" onClick={() => setShowSide(false)}>
                          <FontAwesomeIcon icon={faNoteSticky} />
                        </Button>
                      ) : (
                        <Button
                          title="Post"
                          onClick={() => presentarUsuarios(item.id, presentarPost(item.id))}
                        >
                          <FontAwesomeIcon icon={faNoteSticky} />
                        </Button>
                      )}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </ContenedorTabla>
      </Content>
    </Fragment>
  );
};

export default Usuarios;
