import { Fragment, React, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDeleteLeft, faXmarkCircle } from "@fortawesome/free-solid-svg-icons";
import {
  SideBar,
  SideBody,
  SideHead,
  PostTable,
  Button,
} from "../../styledComponents/styleComponentSide";
import { ContenedorTabla } from "../../styledComponents/styleComponentUsers";

const Side = (props) => {
  const usuarioArray = props.currentId;
  console.log("Entré en side: ", usuarioArray);
  const postArray = props.setPostId;
  console.log("Soy los post del usuario: ", postArray);

  const [postDatas, setpostData] = useState(postArray);
  const eliminarPhost = (id) => {
    setpostData(postArray.filter((pst) => pst.id !== id));
    console.log("Eliminar", setpostData);
  };

  return (
    <Fragment>
      <SideBar>
        <SideHead>
          <div>
            <Button
              onClick={() => {
                props.setShowSide(false);
              }}
            >
              <FontAwesomeIcon icon={faXmarkCircle} />
            </Button>
          </div>
        </SideHead>
        <SideBody>
          <ContenedorTabla>
            <table>
              <tbody>
                {usuarioArray.map((item) => (
                  <tr key={item.id}>
                    <td>
                      <div>
                        <img src={item.avatar} />
                        <b>
                          {item.first_name} {item.last_name}
                        </b>
                        <span>
                          <i>{item.email}</i>
                        </span>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </ContenedorTabla>
          <hr />
          <PostTable>
            <table>
              <tbody>
                {postDatas.map((item, index) => (
                  <tr key={item.id}>
                    <td>{index + 1}</td>
                    <td>{item.title}</td>
                    <td>
                      <Button
                        title="Eliminar Post"
                        onClick={() => eliminarPhost(item.id)}
                      >
                        <FontAwesomeIcon icon={faDeleteLeft} />
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </PostTable>
        </SideBody>
      </SideBar>
    </Fragment>
  );
};

export default Side;
