import { Fragment, React, useState, useNavigate} from "react";
import {
  Div,
  H2,
  Form,
  Label,
  Input,
  Button,
  Body,
  ALink,
  IMG,
} from "../styledComponents/stylesComponentsLogin";
import lInicio from "../images/logo1.png";

import loginSevice from "../redux/loginService";

const Login = () => {
  const [email, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [stateUserLogin, setUserLogin] = useState(null);
  const [stateError, setError] = useState(null);


  const iniciarSesion = async (event) => {
    // event.preventDefault();
    // try {

    //   const userLogin = await loginSevice.login({
    //     email,
    //     password,
    //   });

    //   setUserLogin(userLogin);
    //   setUserName("");
    //   setPassword("");
      
    //   console.log("Inicié Sesión: ", userLogin);
    // } catch (err) {
    //   setError("El usuario y/o contraseña son incorrectos");
    //   setTimeout(() => {
    //     setError(null);
    //   }, 5000);

    //   console.log("Error: ", err);
    // }

    
  };

  return (
    <Body>
      <Div>
        <IMG src={lInicio} />
        <H2>Bienvenido(a) </H2>
        <Label>
          Escriba su <b>Usuario</b> y <b>Contraseña</b> para Iniciar Sesión en
          el Sistema, por favor.
        </Label>

        <Form onSubmit={iniciarSesion} action='/usuarios'>
          <Input
            type="text"
            value={email}
            name="Username"
            placeholder="Indique su Usuario"
            onChange={({ target }) => setUserName(target.value)}
          ></Input>
          <Input
            type="password"
            value={password}
            name="Password"
            placeholder="Indique su Contraseña"
            onChange={({ target }) => setPassword(target.value)}
          ></Input>
          <Button type="submit">Iniciar Sesión</Button>
          <ALink href="#">¿Olvidaste tu contraseña?, Recuperala aquí</ALink>
        </Form>
      </Div>
    </Body>
  );
};

export default Login;
