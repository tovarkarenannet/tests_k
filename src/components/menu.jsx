import { React } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookOpenReader, faPhotoFilm, faArrowAltCircleRight } from "@fortawesome/free-solid-svg-icons";
import {
  NavComponente,
  NavWrapper,
  NavLogo,
  NavMenu,
  NavItem,
  NavItemLink,
  IMG,
} from "../styledComponents/styledComponentMenu";
import lInicio from "../images/logo1.png";

const Menu = () => {
  return (
    <NavComponente>
      <NavWrapper>
        <NavMenu>
          <NavLogo>
            <IMG src={lInicio} />
          </NavLogo>
          <NavItem>
            <NavItemLink href="/usuarios"><FontAwesomeIcon icon={faBookOpenReader} /><p>Usuarios</p></NavItemLink>
          </NavItem>
          <NavItem>
            <NavItemLink href="/album"><FontAwesomeIcon icon={faPhotoFilm} /><p>Álbum</p></NavItemLink>
          </NavItem>
          <NavItem>
            <NavItemLink href="/"><FontAwesomeIcon icon={faArrowAltCircleRight} /><p>Cerrar Sesión</p></NavItemLink>
          </NavItem>
        </NavMenu>
      </NavWrapper>
    </NavComponente>
  );
};

export default Menu;
