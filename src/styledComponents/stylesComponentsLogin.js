import styled from "styled-components";
import fondo from '../images/fondo1.jpg';

export const Button = styled.button`
  background: #fff;
  cursor: pointer;
  font-size: 1.1vw;
  color: #054650;
  margin: 2px;
  padding: 4px 8px;
  font-weight: bold;
  border: 2px solid #054650;
  border-radius: 5px;

  &:hover {
    background: #054650;
    color: #fff;
  }
`;

export const H2 = styled.h2`
  color: #054650;
  font-size: 2.5vw;
  display: flex;
  justify-content: center;
  margin-bottom: 40px;
`;

export const Div = styled.div`
  background: rgba(255, 255, 255, 0.829);
  width: 25%;
  height: 83vh;
  margin: 0 auto;
  padding: 4%;
  font-weight: bold;
  border: 2px solid transparent;
  border-radius: 5px;
  text-align: center;

  button{
    width: 103% !important;
    margin: 0 0 20px 0 !important;
  }
`;

export const Form = styled.form`
  display: block;
  justify-content: center;
  margin-top: 25px;
`;

export const Label = styled.label`
  width: 100% !important;
  font-size: 1vw;
  color: #566573;
  font-weight: 500;
`;

export const Input = styled.input`
  width: 100% !important;
  margin: 0 0 15px 0;
  border: 2px solid #cacfd2;
  border-radius: 5px;
  height: 25px;
  color: #16a085;
  font-weight: 600;
  font-size: 1.1vw;

  &:focus,
  [type]:focus {
    box-shadow: 0 2px 2px rgba(0, 0, 0, 0.075) inset,
      0 0 16px rgba(126, 239, 104, 0.6);
    outline: none;
  }

  &::placeholder {
    font-weight: 600;
    color: #16a085;
    font-size: 1.1vw;
  }
`;

export const Body = styled.body`
  background-image: url(${fondo});
`;

export const ALink = styled.a`
  text-decoration: none;
  color: #16a085;
  font-size: 1.1vw;

  &:hover {
    text-decoration: underline;
  }
`;

export const IMG = styled.img`
  width: 60%;
`