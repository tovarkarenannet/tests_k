import styled from "styled-components";
import fondo from "../images/fondo1.jpg";

export const NavComponente = styled.div`
  width: 100%;
  height: 70px !important;
  //background-color: #054650;
  background-image: url(${fondo});
  background-position: top;
  padding: 0 !important;
`;
export const NavWrapper = styled.div`
  width: 100%;
  height: 70px !important;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 0;
`;
export const NavLogo = styled.div`
  justify-content: center;
  padding: 0.5%;
`;
export const NavMenu = styled.ul`
  height: 100%;
  display: flex;
  justify-content: space-between;
  list-style: none;
  margin: 0;
  background-color: #054650;
  opacity: 0.8;
`;
export const NavItem = styled.li`
  height: 100%;
  width: 35%;
`;
export const NavItemLink = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  padding: 0 2.2rem;
  color: #fff;
  font-size: 1rem;
  font-weight: 300;
  cursor: pointer;
  transition: 0.5s all ease;
  text-decoration: none;
  font-weight: bolder;
  font-size: 1.2vw;

  &:hover {
    background: #16a085;
    color: #ecf0f1;
  }

  icon {
    width: 10%;
  }

  p {
    margin-left: 10%;
  }
`;

export const IMG = styled.img`
  width: 11%;
`;
