import styled from "styled-components";

// Estilos de la tabla
export const ContenedorTabla = styled.div`
  display: flex;
  justify-content: center;

  #id {
    text-align: center !important;
  }

  #acctions {
    text-align: center;
    color: #054650;
    &:hover {
      color: #fff;
    }
  }

  table {
    display: block;
    margin: auto auto;
    justify-content: center;
    /* border-spacing: 0 !important;
    border: 2px solid #054650; */
    width: 100%;

    /* thead {
      background: #054650;
      color: #fff;
      th {
        padding: 10px 30px;
        margin: 0;
        width: 50%;
        text-align: left;
      }
    } */

    tr {
      display: inline-block;
      width: 33%;
      border-spacing: 0 !important;
      &:hover {
        cursor: pointer;
      }
      /* 
      &:nth-child(even) {
        background-color: #effefd;
      } */
    }

    td {
      padding: 20px;
      margin: 0;
      width: 50% !important;
    }

    img {
      border-radius: 50%;
      width: 140px;
      height: 140px;
      display: flex;
      margin: auto;
      margin-bottom: 5%;
      border: #054650 solid 5px;
    }

    span {
      display: block;
    }

    div {
      padding: 0 30px;
    }

    h3 {
      margin: 0;
    }

    h5 {
      margin: 0;
    }

    h6 {
      margin: 0;
    }

    #acciones {
      padding: 0 5px;
    }
  }
`;

export const Content = styled.div`
  padding: 0 10% !important;
`;

export const Titulos = styled.h2`
  color: #16a085;
  margin-bottom: 2%;
`;
