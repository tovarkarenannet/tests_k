import styled from "styled-components";

export const SideBar = styled.div`
  z-index: 2;
  width: 30%;
  background-position: top;
  padding: 0 !important;
  margin: 0;
  height: 100%;
  animation-duration: 10s;
  position: absolute;
  transition: width 4s;
`;

export const SideHead = styled.div`
  width: 100%;
  background-color: #054650;
  opacity: 0.9;
  margin: 0 !important;

  div {
    color: #fff;
    font-weight: bolder;
    text-align: right;
    font-size: 2vw;
    padding: 0 5%;
  }

`;

export const SideBody = styled.div`
  width: 100%;
  height: 100%;
  background-color: #054650;
  /* margin: -22px 0 0 0 !important; */
  opacity: 0.8;
  color: #fff;
  position: relative;
  transition: height 4000s;
`;

export const PostTable = styled.div`
  font-size: 1vw;
  padding: 5%;
  overflow-y: scroll;
  height: 250px;

  td {
    padding: 10%, 5%;
  }
`;

export const PostTableAlbum = styled.div`
  font-size: 1vw;
  padding: 5%;
  overflow-y: scroll;
  height: 90%;
  
  img{
    width: 50px;
  }

  td {
    padding: 10%, 5%;
  }
`;

export const Button = styled.button`
  background: #fff;
  cursor: pointer;
  font-size: 1.1vw;
  color: #054650;
  margin: 2px;
  padding: 4px 8px;
  font-weight: bold;
  border: 2px solid #054650;
  border-radius: 5px;

  &:hover {
    background: #054650;
    color: #fff;
  }
`;
