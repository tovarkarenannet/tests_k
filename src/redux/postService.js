import axios from "axios";

//Constantes
const dataInicialPost = {
  arrayPost: []
};

//Types
const OBTENER_POST_EXITO = "OBTENER_POST_EXITO";

//Reducer
export default function postReducer(postEstado = dataInicialPost, postAcciones) {
  switch (postAcciones.type) {
    case OBTENER_POST_EXITO:
      return { ...postEstado, arrayPost: postAcciones.payload };
    default:
      return { ...postEstado };
  }
}

//Acciones {Llamada al api}
export const obtenerPost = () => async (dispatchpost, getState) => {
  try {
    const res = await axios.get("https://jsonplaceholder.typicode.com/posts");
    //res.data.results;

    dispatchpost({
      type: OBTENER_POST_EXITO,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};