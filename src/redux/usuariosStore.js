import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

//Llamando Reducers
import usuarioService from './usuariosServiceD';
import postService from './postService';
import albumService from './albumServiceUsuarios';
import albumUserService from './albumsService';

const rootReducer = combineReducers ({
    usuariosServ: usuarioService,
    postServ: postService,
    albumServ: albumService,
    albumUserServ: albumUserService,
})


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generarUsuarios() {
    const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

    return store;
}