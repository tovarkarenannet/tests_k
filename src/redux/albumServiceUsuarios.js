import axios from "axios";

//Constantes
const dataInicialAlbum = {
  arrayAlbum: [],
};

//Types
const OBTENER_ALBUM_EXITO = "OBTENER_ALBUM_EXITO";

//Reducer
export default function albumsReducer(
  albumEstado = dataInicialAlbum,
  albumAcciones
) {
  switch (albumAcciones.type) {
    case OBTENER_ALBUM_EXITO:
      return { ...albumEstado, arrayAlbum: albumAcciones.payload };
    default:
      return { ...albumEstado };
  }
}

//Acciones {Llamada al api}
export const obtenerAlbums = () => async (dispatchAlbumAcciones, getState) => {
  try {
    const res = await axios.get("https://jsonplaceholder.typicode.com/users");
    //res.data.results;

    dispatchAlbumAcciones({
      type: OBTENER_ALBUM_EXITO,
      payload: res.data,
    });

    console.log("Entre al servicio Album", res.data);
  } catch (err) {
    console.log(err);
  }
};
