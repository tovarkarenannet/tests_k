import axios from "axios";

const urlLogin = 'https://reqres.in/api/login';

const login = async credentials => {
    const {res} = await axios.post(urlLogin, credentials);

    return res,  console.log('Me autorizas?', res);
}

export default {login}