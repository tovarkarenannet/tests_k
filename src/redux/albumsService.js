import axios from "axios";

//Constantes
const dataInicialAlbumOfUser = {
  arrayAlbumOfUser: [],
};

//Types
const OBTENER_ALBUM_EXITO_USER = "OBTENER_ALBUM_EXITO_USER";

//Reducer
export default function albumsUserReducer(
  albumEstadoUser = dataInicialAlbumOfUser,
  albumAccionesUser
) {
  switch (albumAccionesUser.type) {
    case OBTENER_ALBUM_EXITO_USER:
      return { ...albumEstadoUser, arrayAlbumOfUser: albumAccionesUser.payload };
    default:
      return { ...albumEstadoUser };
  }
}

//Acciones {Llamada al api}
export const obtenerAlbumsUser = () => async (dispatchAlbumAccionesUser, getState) => {
  try {
    const res = await axios.get("https://jsonplaceholder.typicode.com/photos");
    //res.data.results;

    dispatchAlbumAccionesUser({
      type: OBTENER_ALBUM_EXITO_USER,
      payload: res.data,
    });

    console.log("Entre al servicio Album de Usuario", res.data);
  } catch (err) {
    console.log(err);
  }
};
