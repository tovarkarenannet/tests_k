import axios from "axios";

//Constantes
const dataInicial = {
  arrayUsuarios: []
};

//Types
const OBTENER_USUARIOS_EXITO = "OBTENER_USUARIOS_EXITO";

//Reducer
export default function usuariosReducer(uEstado = dataInicial, uAcciones) {
  switch (uAcciones.type) {
    case OBTENER_USUARIOS_EXITO:
      return { ...uEstado, arrayUsuarios: uAcciones.payload };
    default:
      return { ...uEstado };
  }
}

//Acciones {Llamada al api}
export const obtenerUsuarios = () => async (dispatch, getState) => {
  try {
    const res = await axios.get("https://reqres.in/api/users");
    //res.data.results;

    dispatch({
      type: OBTENER_USUARIOS_EXITO,
      payload: res.data.data,
    });

    console.log("Entre al servicio");
  } catch (err) {
    console.log(err);
  }
};