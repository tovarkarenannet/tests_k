import { React, Fragment } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { Provider } from "react-redux";
import generarUsuarios from "./redux/usuariosStore";

//Componentes
import Usuarios from "./components/Usuarios/usuarios";
import Login from "./components/login";
import Menu from "./components/menu";
import Album from "./components/Albums/album";

function App() {
  const store = generarUsuarios();
  return (
    <Fragment>
      <Router>
        <Switch>
          <Route path="/" exact>
            <Login />
          </Route>
          <Route path="/usuarios" exact>
            <Provider store={store}>
              <Menu />
              <Usuarios />
            </Provider>
          </Route>
          <Route path="/album" exact>
            <Provider store={store}>
              <Menu />
              <Album />
            </Provider>
          </Route>
        </Switch>
      </Router>
    </Fragment>
  );
}

export default App;
