#Requerimiento para levantar el proyecto.
Necesita tener la versión de node js: 16.3.1,

#Descargar el proyecto
En una carpeta, descargar el proyecto con el siguiente comando:
    git clone https://gitlab.com/tovarkarenannet/tests_k.git

#Levantar proyecto
- Una vez descargado el proyecto, posicionarte en la carpeta tests_k
- Instalar la librerias de node:
    -> npm install
- Correr el proyecto con los comandos:
    -> npm run start o npm start


Nota: Se me acabo el tiempo por lo que el login no lo terminé y el api de edición traía datos distintos a los que se presentaban en la tabla pero espero les agrade, me gustó mucho el examen, gracias por el reto. ¡Saludos!